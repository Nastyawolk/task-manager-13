package ru.t1.volkova.tm.api.service;

import ru.t1.volkova.tm.api.repository.ITaskRepository;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}
